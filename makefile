run: server.cpp files.o mongoose.o
	g++ server.cpp files.o mongoose.o -o server -pthread
	
files.o: files.cpp files.h
	g++ files.cpp -c

mongoose.o: mongoose.c mongoose.h
	g++ mongoose.c -c
