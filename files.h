#include <sys/types.h>
#include <dirent.h>
#include <errno.h>
#include <vector>
#include <string>
#include <iostream>
#if !defined(FILES_H_)
#define FILES_H_
class FILES {       // The class
  public:             // Access specifier
    int GetFiles(std::string dir, std::vector<std::string> &files);  // Attribute (string variable)
};
#endif // FILES_H_