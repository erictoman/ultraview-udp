#define MG_ENABLE_HTTP_STREAMING_MULTIPART 1
#include "mongoose.h"
#include <stdio.h>
#include <iostream>
#include <string>
#include <iostream>
#include "files.h"
#include <thread>

using namespace std;

static const char *s_http_port = "8000";
static struct mg_serve_http_opts s_http_server_opts;

static void handle_size(struct mg_connection *nc, struct http_message *hm) {
		FILES FileGetter = FILES();
		vector<string> files = vector<string>();
		FileGetter.GetFiles("./www/Imagenes",files);
		/*
		char ip1_c[256];
		char ip2_c[256];
		char res[256];
		mg_get_http_var(&hm->body, "ip1",ip1_c,sizeof(ip1_c));
		mg_get_http_var(&hm->body, "ip2",ip2_c,sizeof(ip2_c));
		mg_get_http_var(&hm->body, "res",res,sizeof(res));
		cout<<"resolución: "<<atoi(res)<<endl;
		int ip1=0;
		int ip2=0;
		ip1=atoi(ip1_c);
		ip2=atoi(ip2_c);
		*/
		string cadena ="";
		for (int i =0;i<files.size();i++) {
			if(files[i]!="." && files[i]!=".."){
				cadena=cadena+",/Imagenes/"+files[i];
			}
    	}
		int n = cadena.length();
		char char_array[n + 1];
		strcpy(char_array, cadena.c_str());
		mg_send_head(nc,200,n, "Content-Type: text/plain");
		mg_printf(nc, "%s", char_array);
}

static void ev_handler(struct mg_connection *nc, int ev, void *p) {
	char query[256];
 	struct http_message *hm = (struct http_message *) p;
	if (ev == MG_EV_HTTP_REQUEST) {
		if(mg_vcmp(&hm->uri, "/ObtenerImagenes") == 0){
			handle_size(nc, hm);
		}else{
			mg_serve_http(nc, (struct http_message *) p, s_http_server_opts);
		}
	}
}


void executer(int t,string ip,string ipF,string calidad){
	while(true){
		string del =".";
		string token = ip.substr(0,ip.find_last_of(del));
		string final = ip.substr(ip.find_last_of(del)+1,ip.length());
		for(int i =stoi(final);i<stoi(ipF)+stoi(final);i++){
			system(("./UDP/Server "+token+"."+to_string(i)+" 3737 "+calidad).c_str());
			cout<<"./UDP/Server "+token+"."+to_string(i)+" 3737 "+calidad.c_str()<<endl;
		}
		sleep(t);
	}
}

int main(int argc,char * argv[]) {
	//t
	//IP
	//Limite
	//Calidad
	thread first (executer,atoi(argv[1]),argv[2],argv[3],argv[4]);
	struct mg_mgr mgr;
	struct mg_connection *nc;
	mg_mgr_init(&mgr, NULL);
	printf("Servidor en puerto: %s\n", s_http_port);
	nc = mg_bind(&mgr, s_http_port, ev_handler);
	if (nc == NULL) {
		printf("Failed to create listener\n");
		return 1;
	}
	mg_set_protocol_http_websocket(nc);
	s_http_server_opts.document_root = "www"; //Directorio actual
	s_http_server_opts.enable_directory_listing = "yes";
	for (;;) {
		mg_mgr_poll(&mgr, 1000);
		//first.join();
	}
	mg_mgr_free(&mgr);
	return 0;
}