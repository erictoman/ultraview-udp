run: servidor.cpp SocketDatagrama.o PaqueteDatagrama.o FileSaver.o
	g++ servidor.cpp SocketDatagrama.o PaqueteDatagrama.o FileSaver.o -o servidor
	
SocketDatagrama.o: SocketDatagrama.cpp SocketDatagrama.h
	g++ SocketDatagrama.cpp -c

PaqueteDatagrama.o: PaqueteDatagrama.cpp PaqueteDatagrama.h
	g++ PaqueteDatagrama.cpp -c

FileSaver.o: PaqueteDatagrama.cpp PaqueteDatagrama.h
	g++ FileSaver.cpp PaqueteDatagrama.cpp -c