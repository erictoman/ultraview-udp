#include "PaqueteDatagrama.h"
#include "SocketDatagrama.h"
#include <vector>
#include "FileSaver.h"
#include <string.h>
#include <iostream>
using namespace std;

int main(int argc, char* argv[]){
    SocketDatagrama server = SocketDatagrama(0);
    PaqueteDatagrama init = PaqueteDatagrama(3);
    init.inicializaIp(argv[1]);
    init.inicializaPuerto(atoi(argv[2]));
    init.inicializaDatos(argv[3]);
    cout<<"Calidad: "<<init.obtieneDatos()<<endl;
    if(server.envia(init)){
        cout << "Server iniciado" << endl;
        int i=0;
        string nombre;
        vector <PaqueteDatagrama> paquetes;
        while(i==0) {
            PaqueteDatagrama paquete = PaqueteDatagrama(60000);
            int n;
            if((n=server.recibe(paquete))>0) {
                cout << "\nMensaje recibido de:" << endl;
                cout << "IP: " << paquete.obtieneDireccion() << endl;
                cout << "Puerto: " << paquete.obtienePuerto() << endl;
                cout << "Longitud: " << n << endl;
                if(n==1){
                    i=1;
                }else{
                    PaqueteDatagrama aux = PaqueteDatagrama(n);
                    aux.inicializaDatos(paquete.obtieneDatos());
                    paquetes.push_back(aux);
                }
            }
            nombre=paquete.obtieneDireccion();
        }
        FileSaver f = FileSaver();
        f.saveFile(string("./www/Imagenes/"+nombre+".jpg").c_str(),paquetes);
    }
}