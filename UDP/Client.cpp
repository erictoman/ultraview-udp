#include "PaqueteDatagrama.h"
#include "SocketDatagrama.h"
#include "./scrot/scrotSaver.h"
#include "FileOpener.h"
#include <string.h>
#include <unistd.h>
#include <iostream>
using namespace std;

int main(int argc, char* argv[]){
    SocketDatagrama client = SocketDatagrama(3737);
    PaqueteDatagrama listener = PaqueteDatagrama(3);
    scrotSaver s = scrotSaver();
    while (true){
        int n =0;
        if(n=(client.recibe(listener))){
            int res=atoi(listener.obtieneDatos());
            cout<<"AQUI "<<n<<" AQUI "<<res<<endl;
            if(listener.obtieneLongitud()==3){
                s.saveScreen(res,"./IMG/default.jpg");
                FileOpener f = FileOpener();
                vector <char> datos = f.getInfo("./IMG/default.jpg");
                int paq_to_send = datos.size()/60000;
                int customs = datos.size()%60000;
                char * buffer = datos.data();
                if(customs>0){
                    paq_to_send++;
                }
                for(int i = 0;i<paq_to_send;i++){
                    if(i==paq_to_send-1){
                        char buf[customs];
                        memcpy(buf,buffer+i*60000,customs);
                        PaqueteDatagrama dat = PaqueteDatagrama(customs);
                        dat.inicializaIp(listener.obtieneDireccion());
                        dat.inicializaPuerto(listener.obtienePuerto());
                        dat.inicializaDatos(buf);
                        client.envia(dat);
                    }else{
                        char buf[60000];
                        memcpy(buf,buffer+i*60000,60000);
                        PaqueteDatagrama dat = PaqueteDatagrama(60000);
                        dat.inicializaIp(listener.obtieneDireccion());
                        dat.inicializaPuerto(listener.obtienePuerto());
                        dat.inicializaDatos(buf);
                        client.envia(dat);
                    }
                    cout<<"Paquete "<<i<<endl;
                    sleep(1);
                }
                char * buf="A";
                PaqueteDatagrama dat = PaqueteDatagrama(1);
                dat.inicializaIp(listener.obtieneDireccion());
                dat.inicializaPuerto(listener.obtienePuerto());
                dat.inicializaDatos(buf);
                client.envia(dat);
            }
        }
    }
    return 0;
}