#include "PaqueteDatagrama.h"
#include "SocketDatagrama.h"
#include <vector>
#include "FileSaver.h"
#include <string.h>
#include <iostream>
using namespace std;

int main(int argc, char* argv[]){
    SocketDatagrama server = SocketDatagrama(atoi(argv[1]));
    cout << "Server iniciado" << endl;
    int i=0;
    vector <PaqueteDatagrama> paquetes;
    while(i==0) {
        PaqueteDatagrama paquete = PaqueteDatagrama(60000);
        int n;
        if((n=server.recibe(paquete))>0) {
        	cout << "\nMensaje recibido de:" << endl;
        	cout << "IP: " << paquete.obtieneDireccion() << endl;
        	cout << "Puerto: " << paquete.obtienePuerto() << endl;
        	cout << "Longitud: " << n << endl;
            if(n==1){
                i=1;
            }else{
                PaqueteDatagrama aux = PaqueteDatagrama(n);
                aux.inicializaDatos(paquete.obtieneDatos());
                paquetes.push_back(aux);
            }
        }
    }
    FileSaver f = FileSaver();
    f.saveFile("ejemplo.jpg",paquetes);
}