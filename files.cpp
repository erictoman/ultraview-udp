#include "files.h"
using namespace std;

int FILES::GetFiles(string dir, vector<string> &files){
    DIR *dp;
    struct dirent *dirp;
    if((dp = opendir(dir.c_str())) == NULL) {
        cout << "Error(" << errno << ") opening " << dir << endl;
        return errno;
    }
    while ((dirp = readdir(dp)) != NULL) {
        files.push_back(string(dirp->d_name));
    }
    closedir(dp);
    return 0;
}
/*
int main(){
    string dir = string("./www/imagenes");
    vector<string> files = vector<string>();
    getdir(dir,files);
    for (unsigned int i = 0;i < files.size();i++) {
        if(files[i].size()>5){
            cout << files[i] << endl;
        }
    }
    return 0;
}
*/